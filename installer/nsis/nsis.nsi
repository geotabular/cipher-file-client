Name "Cipher File"
OutFile "setup-cipher-file-x64.exe"
RequestExecutionLevel admin

Function .onInit
# Extract files.
SetOutPath $EXEDIR
Banner::show "Cipher File: Loading Setup Files..."
# Extract rest
File /r "cipher-file-izpack.jar"
File /r "desktop-icon.ico"

CreateDirectory "$TEMP\CipherFileSetup"
SetOutPath "$TEMP\CipherFileSetup"
File /r "jre"

Banner::destroy
# Run setup.
nsExec::Exec "$TEMP\CipherFileSetup\jre\bin\java.exe -jar $EXEDIR\cipher-file-izpack.jar"

CopyFiles /SILENT "$TEMP\CipherFileSetup\jre" "$PROGRAMFILES64\Cipher File"

CreateShortCut "$DESKTOP\Cipher File.lnk" "$PROGRAMFILES64\Cipher File\cipher-file-client.exe" "" "$EXEDIR\desktop-icon.ico"

# Exit.
Abort

FunctionEnd
Section
SectionEnd