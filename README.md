# Cipher File

Cipher File is an open source encryption tool that allows you to encrypt files in the AES encryption standard. With CipherFile you can protect sensitive data on any Windows PC, Desktop, Laptop, Hard Disk or Removable Drive such as USB Flash Drive, Memory Stick, etc.  Cipherfile is free open source software brought to you by Geotabular Pty Ltd.

Cipher File uses a password based, key file encryption method. Password key file is generated using:

- **Algorithm**: AES
- **Mode**: CBC
- **Padding**: PKCS5PADDING
- **Delegate**: PBKDF2WithHmacSHA256
- **Derived** Key Length: 256 bit (32 bytes)
- **Random** IV Length: 128 bit (16 bytes)
- **Random** Salt Length: 64 bit (8 bytes)
- **Iterations**: 65536

Key is then used to encrypt the data file using:

- **Algorithm**: AES
- **Transformation**: AES
- **Key Length**: 128 bit (32 bytes)

![Cipher File Encryption Methodology](https://bytebucket.org/geotabular/cipher-file-client/raw/5e155ba2a61e014eb632b53276d6a4d190054baa/encryption-diagram.png)

More information at: http://www.cipherfile.com.au/

# Cipher File - Developer Setup

## Requirements

- Gradle 2.1+
- JDK 1.8+
- NSIS 2.51+ (http://nsis.sourceforge.net/Download)


## Building

### Windows

- run make-windows.bat
- setup-cipher-file-x86.exe will be located in build/izpack