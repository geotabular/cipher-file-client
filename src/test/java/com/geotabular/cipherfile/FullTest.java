package com.geotabular.cipherfile;


import com.geotabular.cipherfile.compression.Compressor;
import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import com.geotabular.cipherfile.encryption.Encryptor;
import com.geotabular.cipherfile.encryption.KeyCreator;
import com.geotabular.cipherfile.encryption.ProgressCallback;
import com.geotabular.cipherfile.splitting.Splitter;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class FullTest {

    private static final ProgressCallback PROGRESS_CALLBACK =
            (count, size, status) -> System.out.println(status.name() + " - " + count + " of " + size);

    public static void main(String[] args) throws IOException {

        //SPLIT
        final Splitter splitter = new Splitter();
        final File file = new File(ClassLoader.getSystemResource("csv-data.csv").getFile());

        final String md5Hash = getMd5Hash(file);

        final long length = file.length();
        final List<String> split = splitter.split(file.getAbsolutePath(), 500000, PROGRESS_CALLBACK);
        assert split.size() == 25;

        //ENCRYPT
        final Encryptor encryptor = new Encryptor(new UserConfiguration());
        final KeyCreator keyCreator = new KeyCreator(new UserConfiguration());
        final byte[] bytes = keyCreator.create("test-password", new File(split.get(0)).getParent() + File.separator + "csv-data.cfk");
        final List<String> encrypt = encryptor.encrypt(split, bytes, PROGRESS_CALLBACK);

        assert encrypt.size() == 25;

        //COMPRESS
        final Compressor compressor = new Compressor();
        final String zipFile = new File(encrypt.get(0)).getParent() + File.separator + "csv-data.zip";
        compressor.zip(zipFile, encrypt, PROGRESS_CALLBACK);

        //UNCOMPRESS
        final List<String> unZipped = compressor.unZip(zipFile, new File(zipFile).getParent(), PROGRESS_CALLBACK);

        //DECRYPT
        final List<String> decrypt = encryptor.decrypt(unZipped, bytes, PROGRESS_CALLBACK);

        //JOIN
        final String firstFile = decrypt.get(0);
        final String resultFile = firstFile.substring(0, firstFile.lastIndexOf("."));
        splitter.join(resultFile, PROGRESS_CALLBACK);

        //CHECK
        final File result = new File(resultFile);
        assert result.exists() && result.length() == length;
        final String resultMd5Hash = getMd5Hash(result);
        assert md5Hash.equals(resultMd5Hash);

        //CLEAN UP
        final File tmpDir = new File(result.getParentFile().getParent());
        FileUtils.deleteDirectory(tmpDir);


    }

    private static String getMd5Hash(File file) throws IOException {
        final FileInputStream fis = new FileInputStream(file);
        final String md5 = DigestUtils.md5Hex(fis);
        fis.close();
        return md5;
    }

}
