package com.geotabular.cipherfile.i18n;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ResourceBundle;

@Service
@Scope("singleton")
public class ResourceBundleMessages implements Messages {

    private ResourceBundle labels;

    @PostConstruct
    public void postConstruct(){
        labels = ResourceBundle.getBundle("i18n/messages");
    }


    @Override
    public String getString(String key) {
        return labels.getString(key);
    }

    @Override
    public ResourceBundle getResourceBundle() {
        return labels;
    }
}
