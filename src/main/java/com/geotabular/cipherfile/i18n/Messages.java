package com.geotabular.cipherfile.i18n;

import java.util.ResourceBundle;

public interface Messages {

    String getString(String key);

    ResourceBundle getResourceBundle();
}
