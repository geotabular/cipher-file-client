package com.geotabular.cipherfile.configuration.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Scope("singleton")
public class JacksonObjectMapperService implements JsonMapperService {

    private ObjectMapper objectMapper;

    @PostConstruct
    public void postConstruct(){
        objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
