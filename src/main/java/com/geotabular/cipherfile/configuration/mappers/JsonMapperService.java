package com.geotabular.cipherfile.configuration.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface JsonMapperService {

    ObjectMapper getObjectMapper();

}
