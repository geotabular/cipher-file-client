package com.geotabular.cipherfile.configuration;


import com.geotabular.cipherfile.configuration.model.UserConfiguration;

public interface ConfigurationService {

    UserConfiguration getUserConfiguration();

    void saveUserConfiguration(UserConfiguration userConfiguration);
}
