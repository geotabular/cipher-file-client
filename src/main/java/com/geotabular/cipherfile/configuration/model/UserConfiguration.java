package com.geotabular.cipherfile.configuration.model;


public class UserConfiguration {


    private KeyCipherConfiguration keyCipherConfiguration = new KeyCipherConfiguration();

    private FileCipherConfiguration fileCipherConfiguration = new FileCipherConfiguration();

    public KeyCipherConfiguration getKeyCipherConfiguration() {
        return keyCipherConfiguration;
    }

    public void setKeyCipherConfiguration(KeyCipherConfiguration keyCipherConfiguration) {
        this.keyCipherConfiguration = keyCipherConfiguration;
    }

    public FileCipherConfiguration getFileCipherConfiguration() {
        return fileCipherConfiguration;
    }

    public void setFileCipherConfiguration(FileCipherConfiguration fileCipherConfiguration) {
        this.fileCipherConfiguration = fileCipherConfiguration;
    }
}
