package com.geotabular.cipherfile.configuration.model;


import java.util.Arrays;
import java.util.List;

public class KeyCipherConfiguration {

    private String algorithm = "AES";
    private String mode = "CBC";
    private String padding = "PKCS5PADDING";
    private String delegate = "PBKDF2WithHmacSHA256";
    private int iterations = 65536;
    private int ivLength = 16;
    private int saltLength = 8;
    private int encryptedPasswordLength = 32;
    private int derivedKeyLength = 64;

    public List<Integer> getKeyLengthChoices() {
        return keyLengthChoices;
    }

    public void setKeyLengthChoices(List<Integer> keyLengthChoices) {
        this.keyLengthChoices = keyLengthChoices;
    }

    private List<Integer> keyLengthChoices = Arrays.asList(32, 48, 64);

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }

    public String getDelegate() {
        return delegate;
    }

    public void setDelegate(String delegate) {
        this.delegate = delegate;
    }

    public int getIvLength() {
        return ivLength;
    }

    public void setIvLength(int ivLength) {
        this.ivLength = ivLength;
    }

    public int getSaltLength() {
        return saltLength;
    }

    public void setSaltLength(int saltLength) {
        this.saltLength = saltLength;
    }

    public int getEncryptedPasswordLength() {
        return encryptedPasswordLength;
    }

    public void setEncryptedPasswordLength(int encryptedPasswordLength) {
        this.encryptedPasswordLength = encryptedPasswordLength;
    }

    public int getDerivedKeyLength() {
        return derivedKeyLength;
    }

    public void setDerivedKeyLength(int derivedKeyLength) {
        this.derivedKeyLength = derivedKeyLength;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }
}
