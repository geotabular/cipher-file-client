package com.geotabular.cipherfile.configuration;


import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import com.geotabular.cipherfile.dialogs.AlertDisplayService;
import com.geotabular.cipherfile.i18n.Messages;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.geotabular.cipherfile.configuration.mappers.JsonMapperService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@Service
@Scope("singleton")
public class JsonConfigurationService implements ConfigurationService{

    @Autowired
    private AlertDisplayService alertDisplayService;

    @Autowired
    private JsonMapperService jsonMapperService;

    @Autowired
    private Messages messages;

    private static final String configurationFileName = "cipher-file.config";

    private static final String configurationPath = System.getProperty("user.home") + File.separator + ".cipher-file";

    private UserConfiguration userConfiguration;
    private File configurationFile;


    @PostConstruct
    public void postConstruct(){
        initConfigDirectory();
        configurationFile = new File(configurationPath + File.separator + configurationFileName);
        if(configurationFile.exists()){
            try {
                this.userConfiguration = jsonMapperService.getObjectMapper().readValue(configurationFile, UserConfiguration.class);
            } catch (IOException e) {
                alertDisplayService.showExceptionAndExit(e,
                        String.format(messages.getString("error.dialog.bad.config"), configurationFile.getAbsolutePath()));
            }
        } else {
            final UserConfiguration defaultConfiguration = new UserConfiguration();
            this.saveUserConfiguration(defaultConfiguration);
            this.userConfiguration = defaultConfiguration;
        }

    }

    private void initConfigDirectory() {
        final File configFilePath = new File(configurationPath);
        if(!configFilePath.exists()){
            final boolean mkdirs = configFilePath.mkdirs();
            if(!mkdirs){
                alertDisplayService.showNonModalConfirm(messages.getString("configuration.load.error"))
                        .map(ButtonType::getButtonData)
                        .filter(ButtonBar.ButtonData.OK_DONE::equals)
                        .ifPresent(buttonData -> System.exit(0));
            }
        }
    }

    @Override
    public UserConfiguration getUserConfiguration() {
        return userConfiguration;
    }

    @Override
    public void saveUserConfiguration(UserConfiguration userConfiguration) {
        try {
            jsonMapperService.getObjectMapper().writeValue(configurationFile, userConfiguration);
        } catch (IOException e) {
            alertDisplayService.showNonModalConfirm(messages.getString("configuration.access.error"))
                    .map(ButtonType::getButtonData)
                    .filter(ButtonBar.ButtonData.OK_DONE::equals)
                    .ifPresent(buttonData -> System.exit(0));
        }
    }
}
