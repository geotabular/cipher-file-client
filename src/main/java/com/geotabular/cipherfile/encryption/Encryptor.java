package com.geotabular.cipherfile.encryption;


import com.geotabular.cipherfile.configuration.model.FileCipherConfiguration;
import com.geotabular.cipherfile.configuration.model.KeyCipherConfiguration;
import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import com.geotabular.cipherfile.encryption.exceptions.FileEncryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyDecryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyEncryptionException;
import com.geotabular.cipherfile.encryption.exceptions.ZeroByteFileException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Encryptor {

    static final String FILE_EXTENSION = ".cfx";
    static final String KEY_FILE_EXTENSION = ".cfk";
    private static final String ENCRYPTED_FILE_EXTENSION = ".ecf";
    private final UserConfiguration userConfiguration;

    public Encryptor(UserConfiguration userConfiguration){
        this.userConfiguration = userConfiguration;
    }


    public List<String> encrypt(final List<String> files, final byte[] key, final ProgressCallback callback){

        final int size = files.size();
        return IntStream.range(0, size)
                .parallel()
                .peek(index -> callback.onProgress(index + 1, size, ProgressCallback.STATUS.Encrypting))
                .mapToObj(index -> encrypt(key, new File(files.get(index)), new File(files.get(index) + ENCRYPTED_FILE_EXTENSION)))
                .collect(Collectors.toList());

    }

    public List<String> decrypt(final List<String> files, final byte[] key, final ProgressCallback callback){

        final int size = files.size();
        return IntStream.range(0, size)
                .parallel()
                .peek(index -> callback.onProgress(index + 1, size, ProgressCallback.STATUS.Decrypting))
                .mapToObj(index -> decrypt(key,
                        new File(files.get(index)),
                        new File(files.get(index).substring(0,
                                files.get(index).lastIndexOf(".") <= 0 ? files.get(index).length() :
                                        files.get(index).lastIndexOf(".")))))
                .collect(Collectors.toList());

    }

    private String encrypt(byte[] key, File inputFile, File outputFile) {

        return doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);

    }

    private String decrypt(byte[] key, File inputFile, File outputFile) {

        return doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);

    }


    private String doCrypto(int cipherMode, byte[] key, File inputFile,
                                 File outputFile) {
        try {
            final Cipher cipher = createFileCipher(cipherMode, key);
            final FileInputStream inputStream = new FileInputStream(inputFile);
            final byte[] inputBytes = new byte[(int) inputFile.length()];
            final int read = inputStream.read(inputBytes);
            if(read == 0){
                throw new ZeroByteFileException();
            }
            final byte[] outputBytes = cipher.doFinal(inputBytes);
            final FileOutputStream outputStream = new FileOutputStream(outputFile);

            outputStream.write(outputBytes);
            inputStream.close();
            outputStream.close();

            return outputFile.getCanonicalPath();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException ex) {
            throw new FileEncryptionException();
        }
    }

    private Cipher createFileCipher(int cipherMode, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {

        final FileCipherConfiguration fileCipherConfiguration = userConfiguration.getFileCipherConfiguration();
        final Key secretKey = new SecretKeySpec(key, fileCipherConfiguration.getAlgorithm());
        final Cipher cipher = Cipher.getInstance(fileCipherConfiguration.getTransformation());
        cipher.init(cipherMode, secretKey);
        return cipher;

    }

    byte[] encryptKey(byte[] key2, byte[] key1, byte[] iv) {

        final KeyCipherConfiguration keyCipherConfiguration = userConfiguration.getKeyCipherConfiguration();
        final Key secretKey = new SecretKeySpec(key2, keyCipherConfiguration.getAlgorithm());
        final Cipher cipher;
        try {
            cipher = Cipher.getInstance(buildAlgorithm(keyCipherConfiguration));
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
            return cipher.doFinal(key1);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException |
                InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            throw new KeyEncryptionException();
        }

    }

    private String buildAlgorithm(KeyCipherConfiguration keyCipherConfiguration) {

        final StringBuilder algorithm = new StringBuilder(keyCipherConfiguration.getAlgorithm());
        if(keyCipherConfiguration.getMode() != null && keyCipherConfiguration.getPadding() != null){
            algorithm.append("/")
                    .append(keyCipherConfiguration.getMode())
                    .append("/")
                    .append(keyCipherConfiguration.getPadding());
        }
        return algorithm.toString();

    }

    byte[] decryptKey(byte[] key1, byte[] key2, byte[] iv) {

        final KeyCipherConfiguration keyCipherConfiguration = userConfiguration.getKeyCipherConfiguration();
        final Key secretKey = new SecretKeySpec(key2, keyCipherConfiguration.getAlgorithm());
        final Cipher cipher;
        try {
            cipher = Cipher.getInstance(buildAlgorithm(keyCipherConfiguration));
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            return cipher.doFinal(key1);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException |
                InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            throw new KeyDecryptionException();
        }

    }
}
