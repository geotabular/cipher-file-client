package com.geotabular.cipherfile.encryption;

import com.geotabular.cipherfile.compression.Compressor;
import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import com.geotabular.cipherfile.encryption.exceptions.FileEncryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyDecryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyFileAccessException;
import com.geotabular.cipherfile.encryption.exceptions.ZeroByteFileException;
import com.geotabular.cipherfile.i18n.Messages;
import com.geotabular.cipherfile.splitting.Splitter;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class DecryptionTask extends Task<Void> {

    public static final String TEMP_FOLDER_NAME = ".cff";
    private final String fileName;
    private final Messages messages;
    private final String tmpDir;
    private final UserConfiguration userConfiguration;
    private String password;
    private boolean skip;
    private static final Logger LOG = LoggerFactory.getLogger(DecryptionTask.class);

    public DecryptionTask(String fileName, Messages messages, UserConfiguration userConfiguration) {
        this.fileName = fileName;
        this.messages = messages;
        this.tmpDir = UUID.randomUUID().toString();
        this.userConfiguration = userConfiguration;
        this.updateMessage(messages.getString("decrypt.status.message.ready"));
        this.updateProgress(0, 0);
    }

    @Override
    protected Void call() {
        try {
            decrypt();
        } catch (Throwable e) {
            LOG.error("Decryption Failed", e);
        }
        return null;
    }

    private void decrypt() throws IOException {

        if(this.skip){
            this.updateMessage(messages.getString("decrypt.status.message.skipped"));
            return;
        }

        final ProgressCallback progressCallback = buildProgressCallback();
        final String outputFolder = new File(fileName).getParent() + File.separator + TEMP_FOLDER_NAME + File.separator + tmpDir;
        final List<String> unCompressedFileParts = new Compressor().unZip(fileName, outputFolder, progressCallback);
        final String keyFileName = buildKeyFileName();
        final byte[] key;
        final List<String> decryptedParts;
        try {
            key = new KeyCreator(userConfiguration).retrieveKey(this.password, keyFileName);
            decryptedParts = new Encryptor(userConfiguration).decrypt(unCompressedFileParts, key, progressCallback);
        } catch (KeyDecryptionException e) {
            this.updateMessage(messages.getString("decrypt.key.status.failed"));
            this.updateProgress(1, 1);
            return;
        } catch (ZeroByteFileException e){
            this.updateMessage(messages.getString("decrypt.file.status.failed.empty"));
            this.updateProgress(1, 1);
            return;
        } catch (FileEncryptionException e){
            this.updateMessage(messages.getString("decrypt.file.status.failed"));
            this.updateProgress(1, 1);
            return;
        } catch (KeyFileAccessException e){
            this.updateMessage(messages.getString("decrypt.key.status.access.failed"));
            this.updateProgress(1, 1);
            return;
        }

        final String part0 = decryptedParts.get(0);
        final String baseFileName = createBaseFileName(part0);
        new Splitter().join(baseFileName, progressCallback);
        final String originalFileName = getOriginalFileName();
        final File destFile = new File(originalFileName);
        final String tmpFolder = destFile.getParent() + File.separator + TEMP_FOLDER_NAME + File.separator + tmpDir;
        final File srcFile = new File(tmpFolder + File.separator + destFile.getName());
        FileUtils.copyFile(srcFile,  destFile);
        final File tmpDirectory = new File(tmpFolder);
        FileUtils.deleteDirectory(tmpDirectory);
        tmpDirectory.getParentFile().deleteOnExit();
        this.updateMessage(messages.getString("decrypt.status.message.done"));


    }

    private String createBaseFileName(String fileName) {
        return fileName.lastIndexOf(".") <= 0 ? fileName : fileName.substring(0, fileName.lastIndexOf("."));
    }

    private ProgressCallback buildProgressCallback() {
        return (int count, int size, ProgressCallback.STATUS status) -> {
            final String statusMessage = messages.getString("decrypt.status.message." + status.name().toLowerCase());
            if(status.equals(ProgressCallback.STATUS.Decrypting)) {
                this.updateMessage(statusMessage);
                this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
            } else {
                this.updateMessage(String.format(messages.getString("decrypt.status.count"), statusMessage, count, size));
                this.updateProgress(count, size);
            }
        };
    }

    public String getOriginalFileName(){

        return new Compressor().getFirstFileName(this.fileName);

    }

    private String buildKeyFileName() {
        return  getOriginalFileName() + Encryptor.KEY_FILE_EXTENSION;
    }

    public String getFileName(){
        return this.fileName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void skip() {
        this.skip = true;
    }
}
