package com.geotabular.cipherfile.encryption;

import com.geotabular.cipherfile.compression.Compressor;
import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import com.geotabular.cipherfile.encryption.exceptions.FileEncryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyEncryptionException;
import com.geotabular.cipherfile.i18n.Messages;
import com.geotabular.cipherfile.splitting.Splitter;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class EncryptionTask extends Task<Void> {

    private final String fileName;
    private final Messages messages;
    private final UserConfiguration userConfiguration;
    private String password;
    private final int chunkSize;

    private static final Logger LOG = LoggerFactory.getLogger(EncryptionTask.class);

    public EncryptionTask(String fileName, int chunkSize, Messages messages, UserConfiguration userConfiguration) {
        this.fileName = fileName;
        this.chunkSize = chunkSize;
        this.messages = messages;
        this.userConfiguration = userConfiguration;
        this.updateProgress(0,0);
        this.updateMessage(messages.getString("encrypt.status.message.ready"));
    }

    @Override
    protected Void call() {
        try {
            encrypt();
        } catch (IOException e) {
            LOG.error("Encryption Failed", e);
        }
        return null;
    }

    private void encrypt() throws IOException {
        final ProgressCallback progressCallback = buildProgressCallback();
        final List<String> fileParts = new Splitter().split(this.fileName, this.chunkSize, progressCallback);
        final String firstFile = fileParts.get(0);
        final String keyFile = buildKeyFileName(firstFile);
        final byte[] key;
        final List<String> encryptedFileParts;
        try {
            key = new KeyCreator(userConfiguration).create(password, keyFile);
            encryptedFileParts = new Encryptor(userConfiguration).encrypt(fileParts, key, progressCallback);

        } catch(KeyEncryptionException e) {
            this.updateMessage(messages.getString("encrypt.key.status.failed"));
            this.updateProgress(1, 1);
            return;
        } catch(FileEncryptionException e){
            this.updateMessage(messages.getString("encrypt.file.status.failed"));
            this.updateProgress(1, 1);
            return;
        }

        final String zipFileName = buildZipFileName();
        new Compressor().zip(zipFileName, encryptedFileParts, progressCallback);
        final File tmpDir = new File(new File(fileParts.get(0)).getParent());
        final File srcFile = new File(keyFile);
        final File destFile = new File(new File(this.fileName).getParent() + File.separator + srcFile.getName());
        FileUtils.copyFile(srcFile, destFile);
        FileUtils.deleteDirectory(tmpDir);
        tmpDir.getParentFile().deleteOnExit();

        this.updateMessage(messages.getString("encrypt.status.message.done"));
    }

    private String buildZipFileName() {
        return this.fileName + ".cfx";
    }

    private ProgressCallback buildProgressCallback() {
        return (int count, int size, ProgressCallback.STATUS status) -> {
            final String statusMessage = messages.getString("encrypt.status.message." + status.name().toLowerCase());
            if(status.equals(ProgressCallback.STATUS.Encrypting)) {
                this.updateMessage(statusMessage);
                this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
            } else {
                this.updateMessage(String.format(messages.getString("encrypt.status.count"), statusMessage, count, size));
                this.updateProgress(count, size);
            }
        };
    }

    private String buildKeyFileName(String name) {
        return name.lastIndexOf(".") <= 0 ? name + Encryptor.FILE_EXTENSION :
                name.substring(0, name.lastIndexOf(".")) + Encryptor.KEY_FILE_EXTENSION;
    }

    public String getFileName(){
        return this.fileName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
