package com.geotabular.cipherfile.encryption;

import com.geotabular.cipherfile.configuration.model.KeyCipherConfiguration;
import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import com.geotabular.cipherfile.encryption.exceptions.KeyDecryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyEncryptionException;
import com.geotabular.cipherfile.encryption.exceptions.KeyFileAccessException;
import org.apache.commons.io.FileUtils;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

public class KeyCreator {

    private static final int BITS_PER_BYTE = 8;
    private final UserConfiguration userConfiguration;

    public KeyCreator(UserConfiguration userConfiguration){
        this.userConfiguration = userConfiguration;
    }

    public byte[] create(String password, String keyFile){

        final byte[] key1 = createRandomKey();
        final byte[] iv = createRandomIv();
        final byte[] salt = createRandomSalt();
        final int keySplitLength = getKeySplitLength();
        final byte[] key2 = new byte[keySplitLength];
        final byte[] key3 = new byte[keySplitLength];

        final byte[] encryptedPasswordKey = createEncryptedPasswordKey(password, salt);

        System.arraycopy(encryptedPasswordKey, 0, key2, 0, keySplitLength);
        System.arraycopy(encryptedPasswordKey, keySplitLength, key3, 0, keySplitLength);

        final byte[] encryptedKey = new Encryptor(userConfiguration).encryptKey(key2, key1, iv);

        writeKeyFile(keyFile, encryptedKey, key3, salt, iv);

        return key1;

    }

    private void writeKeyFile(String keyFile, byte[] encryptedPasswordKey, byte[] key3, byte[] salt, byte[] iv) {
        try {
            final byte[] fileBytes = new byte[encryptedPasswordKey.length + key3.length + salt.length + iv.length];
            System.arraycopy(encryptedPasswordKey, 0, fileBytes, 0, encryptedPasswordKey.length);
            System.arraycopy(key3, 0, fileBytes, encryptedPasswordKey.length, key3.length);
            System.arraycopy(salt, 0, fileBytes, encryptedPasswordKey.length + key3.length, salt.length);
            System.arraycopy(iv, 0, fileBytes, encryptedPasswordKey.length + key3.length + salt.length, iv.length);
            FileUtils.writeByteArrayToFile(new File(keyFile), fileBytes, false);
        } catch (IOException e) {
            throw new KeyFileAccessException();
        }
    }

    private byte[][] readKeyFile(String keyFile, int encryptedKeyLength, int key3Length, int saltLength, int ivLength) {
        try {
            final byte[][] keyParts = new byte[][]{
                    new byte[encryptedKeyLength],
                    new byte[key3Length],
                    new byte[saltLength],
                    new byte[ivLength]
            };
            final byte[] fileBytes = FileUtils.readFileToByteArray(new File(keyFile));
            System.arraycopy(fileBytes, 0, keyParts[0], 0, encryptedKeyLength);
            System.arraycopy(fileBytes, encryptedKeyLength, keyParts[1], 0, key3Length);
            System.arraycopy(fileBytes, encryptedKeyLength + key3Length, keyParts[2], 0, saltLength);
            System.arraycopy(fileBytes, encryptedKeyLength + key3Length + saltLength, keyParts[3], 0, ivLength);
            return keyParts;
        } catch (IOException e) {
            throw new KeyFileAccessException();
        }
    }

    private byte[] createEncryptedPasswordKey(String password, byte[] salt) {
        try {
            final KeyCipherConfiguration keyCipherConfiguration = userConfiguration.getKeyCipherConfiguration();
            final SecretKeyFactory factory = SecretKeyFactory.getInstance(keyCipherConfiguration.getDelegate());
            final KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, keyCipherConfiguration.getIterations(),
                    keyCipherConfiguration.getDerivedKeyLength() * BITS_PER_BYTE);
            final SecretKey tmp = factory.generateSecret(spec);
            final SecretKey secret = new SecretKeySpec(tmp.getEncoded(), keyCipherConfiguration.getAlgorithm());
            return secret.getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new KeyEncryptionException();
        }
    }

    private byte[] createRandomSalt() {
        final SecureRandom secureRandom = new SecureRandom();
        return createRandomBytes(secureRandom, userConfiguration.getKeyCipherConfiguration().getSaltLength());
    }

    private byte[] createRandomBytes(final SecureRandom secureRandom, final int length) {
        byte bytes[] = new byte[length];
        secureRandom.nextBytes(bytes);
        return bytes;
    }

    private byte[] createRandomIv() {
        final SecureRandom secureRandom = new SecureRandom();
        return createRandomBytes(secureRandom, userConfiguration.getKeyCipherConfiguration().getIvLength());
    }

    private byte[] createRandomKey() {
        final SecureRandom secureRandom = new SecureRandom();
        return createRandomBytes(secureRandom, userConfiguration.getFileCipherConfiguration().getKeyLength());
    }

    byte[] retrieveKey(String password, String keyFileName) {

        final KeyCipherConfiguration keyCipherConfiguration = userConfiguration.getKeyCipherConfiguration();
        final int keySplitLength = getKeySplitLength();
        final byte[][] bytes = readKeyFile(keyFileName, keyCipherConfiguration.getEncryptedPasswordLength(), keySplitLength,
                keyCipherConfiguration.getSaltLength(), keyCipherConfiguration.getIvLength());
        final byte[] encryptedKey = bytes[0];
        final byte[] key3 = bytes[1];
        final byte[] salt = bytes[2];
        final byte[] iv = bytes[3];
        final byte[] key2And3 = createEncryptedPasswordKey(password, salt);
        final byte[] key2 = new byte[keySplitLength];
        final byte[] key3Derived = new byte[keySplitLength];

        System.arraycopy(key2And3, 0, key2, 0, key2.length);
        System.arraycopy(key2And3, key2.length, key3Derived, 0, key3Derived.length);

        if(Arrays.equals(key3Derived, key3)){
            return new Encryptor(userConfiguration).decryptKey(encryptedKey, key2, iv);
        } else {
            throw new KeyDecryptionException();
        }


    }

    private int getKeySplitLength() {
        return userConfiguration.getKeyCipherConfiguration().getDerivedKeyLength() / 2;
    }
}
