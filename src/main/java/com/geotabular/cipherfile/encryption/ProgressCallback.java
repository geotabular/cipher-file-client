package com.geotabular.cipherfile.encryption;

public interface ProgressCallback {

    enum STATUS {
        Splitting,
        Encrypting,
        Archiving,
        Expanding,
        Decrypting,
        Joining
    }

    void onProgress(int count, int size, STATUS status);

}
