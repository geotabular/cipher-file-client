package com.geotabular.cipherfile;

import com.geotabular.cipherfile.configuration.ConfigurationService;
import com.geotabular.cipherfile.dialogs.AlertDisplayService;
import com.geotabular.cipherfile.encryption.EncryptionTask;
import com.geotabular.cipherfile.i18n.Messages;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

@Component
class EncryptTabController {

    private TableView<EncryptionTask> table;
    private static final int CHUNK_SIZE = 50000000;

    private TableColumn<EncryptionTask, Double> progressCol;

    @Autowired
    private Messages messages;

    @Autowired
    private FilesTableView<EncryptionTask> filesTableView;

    @Autowired
    private AlertDisplayService alertDisplayService;

    @Autowired
    private ConfigurationService configurationService;

    private AnchorPane encryptTableContainer;

    private Button encryptButton;

    private Button encryptAddFilesButton;

    private EncryptionTask selectedEncryptionTask;

    private Button encryptShowFileButton;

    private Button encryptRemoveFilesButton;

    private TableView<EncryptionTask> createEncryptTaskTable() {

        table = filesTableView.createTableView();

        table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selectedEncryptionTask = newValue;
            encryptShowFileButton.setDisable(selectedEncryptionTask == null);
            encryptRemoveFilesButton.setDisable(selectedEncryptionTask == null);
        });

        final TableColumn<EncryptionTask, String> statusCol = filesTableView
                .createStringColumn("encrypt.file.list.status.column", "message", 200);

        final TableColumn<EncryptionTask, String> fileNameCol = filesTableView
                .createStringColumn("encrypt.file.list.filename.column", "fileName", 200);

        progressCol = new TableColumn<>(messages.getString("encrypt.file.list.progress.column"));
        progressCol.setPrefWidth(200);

        table.getColumns().addAll(fileNameCol, statusCol, progressCol);

        return table;
    }

    void encryptAllFiles(final String pass) {

        Platform.runLater(this::toggleControlsDuringEncrypt);
        final ObservableList<EncryptionTask> items = table.getItems();
        final ExecutorService executor = Executors.newFixedThreadPool(items.size(), this::buildThread);
        items.forEach(task -> task.setPassword(pass));
        items.forEach(encryptionTask -> encryptionTask.setOnSucceeded(event -> onEncryptionTaskSucceeded(items)));
        items.forEach(executor::execute);


    }

    private Thread buildThread(Runnable runnable) {
        final Thread t = new Thread(runnable);
        t.setDaemon(true);
        return t;
    }

    private void toggleControlsAfterEncrypt(){
        this.encryptAddFilesButton.setDisable(false);
    }

    private void toggleControlsDuringEncrypt() {

        this.encryptButton.setDisable(true);
        this.encryptAddFilesButton.setDisable(true);

    }

    private void onEncryptionTaskSucceeded(ObservableList<EncryptionTask> items) {
        if(items.stream().filter(FutureTask::isDone).collect(Collectors.toList()).size() == items.size()) {
            Platform.runLater(this::toggleControlsAfterEncrypt);
            Platform.runLater(this::showEncryptionCompleteDialog);
        }
    }

    private void showEncryptionCompleteDialog() {

        alertDisplayService.showNonModalInfo(messages.getString("encrypt.complete.dialog.header"),
                messages.getString("encrypt.complete.dialog.message"));

    }

    void onClickAddFilesButton(ActionEvent actionEvent) {

        final FileChooser fileChooser = new FileChooser();
        final List<File> files = fileChooser.showOpenMultipleDialog(SpringApplication.getPrimaryStage());
        if(files != null){
            if(table==null) {
                table = createEncryptTaskTable();
                encryptTableContainer.getChildren().add(table);
                table.prefWidthProperty().bind(encryptTableContainer.widthProperty());
                table.prefHeightProperty().bind(encryptTableContainer.heightProperty());

                progressCol.setCellValueFactory(new PropertyValueFactory<>("progress"));
                progressCol.setCellFactory(ProgressBarTableCell.forTableColumn());
            }
            files.forEach(this::addEncryptionTaskToTable);
            encryptButton.setDisable(false);
        }
    }

    public void onClickRemoveFilesButton(ActionEvent actionEvent) {
        final List<String> removals = table.getSelectionModel().getSelectedItems()
                .stream().map(EncryptionTask::getFileName).collect(Collectors.toList());

        final List<EncryptionTask> encryptionTasks = table.getItems()
                .filtered(encryptionTask -> removals.contains(encryptionTask.getFileName()))
                .stream().collect(Collectors.toList());


        for (EncryptionTask encryptionTask : encryptionTasks) {
            table.getItems().remove(encryptionTask);
        }

    }

    private void addEncryptionTaskToTable(File file) {
        try {
            final String canonicalPath = file.getCanonicalPath();
            if(notPresentInTable(canonicalPath)) {
                table.getItems().add(new EncryptionTask(canonicalPath, CHUNK_SIZE, messages,
                        configurationService.getUserConfiguration()));
            }
        } catch (IOException e) {
            alertDisplayService.showNonModalException(e);
        }
    }

    private boolean notPresentInTable(String canonicalPath) {
        return table.getItems()
                .filtered(encryptionTask -> encryptionTask.getFileName().equalsIgnoreCase(canonicalPath))
                .stream().count() == 0;
    }


    void setEncryptTableContainer(AnchorPane encryptTableContainer) {
        this.encryptTableContainer = encryptTableContainer;
    }

    void setEncryptButton(Button encryptButton) {
        this.encryptButton = encryptButton;
    }

    void setEncryptAddFilesButton(Button encryptAddFilesButton) {
        this.encryptAddFilesButton = encryptAddFilesButton;
    }

    public void setEncryptShowFileButton(Button encryptShowFileButton) {
        this.encryptShowFileButton = encryptShowFileButton;
    }

    public void onClickShowFileButton(ActionEvent actionEvent) {
        if (selectedEncryptionTask != null) {
            try {
                Desktop.getDesktop().open(new File(selectedEncryptionTask.getFileName()).getParentFile());
            } catch (IOException e) {
                alertDisplayService.showNonModalException(e);
            }
        }
    }


    public void setEncryptRemoveFilesButton(Button encryptRemoveFilesButton) {
        this.encryptRemoveFilesButton = encryptRemoveFilesButton;
    }

}
