package com.geotabular.cipherfile;

import com.geotabular.cipherfile.i18n.Messages;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class FilesTableView <T>{

    @Autowired
    private Messages messages;

    @Autowired
    private BrowserService browserService;

    public TableView<T> createTableView(){

        TableView<T> table = new TableView<>();

        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        AnchorPane.setBottomAnchor(table, 0.0d);
        AnchorPane.setLeftAnchor(table, 0.0d);
        AnchorPane.setRightAnchor(table, 0.0d);
        AnchorPane.setTopAnchor(table, 0.0d);

        final VBox vBox = createPlaceHolder();
        table.setPlaceholder(vBox);
        return table;

    }

    private VBox createPlaceHolder() {
        final Hyperlink link = new Hyperlink();
        link.setText(messages.getString("encrypt.placeholder.link.text"));
        link.setOnAction(event -> browserService.openBrowser(messages.getString("encrypt.placeholder.link.href")));
        final Label placeHolderLabel = new Label(messages.getString("encrypt.placeholder.label.text"));
        placeHolderLabel.setPadding(new Insets(3, 0, 0, 0));
        final VBox vBox = new VBox();
        final HBox hBox = new HBox();
        hBox.setPadding(new Insets(10,10,10,10));
        hBox.getChildren().addAll(placeHolderLabel, link);
        vBox.getChildren().addAll(hBox);
        return vBox;
    }

    public TableColumn<T, String> createStringColumn(String s, String message, int width) {
        TableColumn<T, String> col = new TableColumn<>(messages.getString(s));
        col.resizableProperty().setValue(true);
        col.setCellValueFactory(new PropertyValueFactory<>(message));
        col.setPrefWidth(width);
        return col;
    }
}
