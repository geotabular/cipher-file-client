package com.geotabular.cipherfile;

import com.geotabular.cipherfile.configuration.ConfigurationService;
import com.geotabular.cipherfile.dialogs.AlertDisplayService;
import com.geotabular.cipherfile.dialogs.DecryptPasswordDialog;
import com.geotabular.cipherfile.dialogs.EncryptPasswordDialog;
import com.geotabular.cipherfile.i18n.Messages;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public class MainController implements InitializingBean {

    @FXML
    public Button decryptAddFilesButton;

    @FXML
    public AnchorPane encryptTableContainer;

    @FXML
    public AnchorPane decryptTableContainer;

    @FXML
    public Button encryptButton;

    @FXML
    public Button encryptAddFilesButton;


    @FXML
    public Button encryptRemoveFilesButton;

    @FXML
    public Button decryptRemoveFilesButton;

    @FXML
    public Button decryptButton;

    @FXML
    public Button encryptShowFileButton;

    @FXML
    public Button decryptShowFileButton;

    @Autowired
    private EncryptTabController encryptTabController;

    @Autowired
    private DecryptTabController decryptTabController;

    @Autowired
    private Messages messages;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private AlertDisplayService alertDisplayService;

    @Override
    public void afterPropertiesSet() throws Exception {
        encryptTabController.setEncryptButton(encryptButton);
        encryptTabController.setEncryptTableContainer(encryptTableContainer);
        encryptTabController.setEncryptAddFilesButton(encryptAddFilesButton);
        encryptTabController.setEncryptShowFileButton(encryptShowFileButton);
        encryptTabController.setEncryptRemoveFilesButton(encryptRemoveFilesButton);

        decryptTabController.setDecryptTableContainer(decryptTableContainer);
        decryptTabController.setDecryptButton(decryptButton);
        decryptTabController.setDecryptAddFilesButton(decryptAddFilesButton);
        decryptTabController.setDecryptShowFileButton(decryptShowFileButton);
        decryptTabController.setDecryptRemoveFilesButton(decryptRemoveFilesButton);
    }

    public void onEncryptClickAddFilesButton(ActionEvent actionEvent){
        try {
            encryptTabController.onClickAddFilesButton(actionEvent);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onEncryptButton(ActionEvent actionEvent) {
        try{
            new EncryptPasswordDialog(configurationService, messages).getPassword().ifPresent(encryptTabController::encryptAllFiles);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onDecryptClickAddFilesButton(ActionEvent actionEvent) {
        try{
            decryptTabController.onClickAddFilesButton(actionEvent);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onClickDecryptButton(ActionEvent actionEvent) {
        try{
            new DecryptPasswordDialog(configurationService, messages).getPassword().ifPresent(decryptTabController::decryptAllFiles);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onShowDecryptedFile(ActionEvent actionEvent) {
        try{
            decryptTabController.onClickShowFileButton(actionEvent);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onShowEncryptedFile(ActionEvent actionEvent) {
        try{
            encryptTabController.onClickShowFileButton(actionEvent);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onEncryptClickRemoveFilesButton(ActionEvent actionEvent) {
        try{
            encryptTabController.onClickRemoveFilesButton(actionEvent);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }

    public void onDecryptClickRemoveFilesButton(ActionEvent actionEvent) {
        try{
            decryptTabController.onClickRemoveFilesButton(actionEvent);
        } catch(Throwable t) {
            alertDisplayService.showNonModalException(t);
        }
    }
}
