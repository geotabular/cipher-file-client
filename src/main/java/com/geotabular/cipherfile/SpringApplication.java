package com.geotabular.cipherfile;

import com.geotabular.cipherfile.i18n.Messages;
import com.gluonhq.ignite.spring.SpringContext;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Collections;

public class SpringApplication extends Application {

    private static Stage primaryStage;

    public static void main(String[] args) {
        BasicConfigurator.configure();
        launch(args);
    }

    private SpringContext context = new SpringContext(this, () -> Collections.singletonList(SpringApplication.class.getPackage().getName()));

    @Autowired
    private Messages messages;

    @Autowired
    private FXMLLoader fxmlLoader;

    static Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        primaryStage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/favicon.png")));

        SpringApplication.primaryStage = primaryStage;
        context.init();
        fxmlLoader.setResources(messages.getResourceBundle());
        fxmlLoader.setLocation(ClassLoader.getSystemResource("main.fxml"));
        final Parent view = fxmlLoader.load();
        InitializingBean initializingBean = fxmlLoader.getController();
        try {
            initializingBean.afterPropertiesSet();
        } catch (Exception e) {
            throw new IOException(e);
        }
        primaryStage.setTitle(messages.getString("title"));
        final Scene scene = new Scene(view);
        scene.getStylesheets().add(ClassLoader.getSystemResource("css/main.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();

    }


}


