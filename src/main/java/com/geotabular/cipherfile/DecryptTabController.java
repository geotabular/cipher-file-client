package com.geotabular.cipherfile;

import com.geotabular.cipherfile.configuration.ConfigurationService;
import com.geotabular.cipherfile.dialogs.AlertDisplayService;
import com.geotabular.cipherfile.dialogs.SkipDialog;
import com.geotabular.cipherfile.encryption.DecryptionTask;
import com.geotabular.cipherfile.i18n.Messages;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

@Component
class DecryptTabController {

    private TableColumn<DecryptionTask, Double> progressCol;

    private TableView<DecryptionTask> table;

    @Autowired
    private FilesTableView<DecryptionTask> filesTableView;

    @Autowired
    private AlertDisplayService alertDisplayService;

    @Autowired
    private ConfigurationService configurationService;


    @Autowired
    private Messages messages;
    private AnchorPane decryptTableContainer;
    private Button decryptButton;
    private Button decryptAddFilesButton;



    private Button decryptRemoveFilesButton;
    private Button decryptShowFileButton;

    private DecryptionTask selectedDecryptionTask;

    private TableView<DecryptionTask> createDecryptTaskTable() {

        table = filesTableView.createTableView();

        table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selectedDecryptionTask = newValue;
            decryptShowFileButton.setDisable(selectedDecryptionTask ==null);
            decryptRemoveFilesButton.setDisable(selectedDecryptionTask == null);

        });

        final TableColumn<DecryptionTask, String> statusCol = filesTableView
                .createStringColumn("decrypt.file.list.status.column", "message", 200);

        final TableColumn<DecryptionTask, String> fileNameCol = filesTableView
                .createStringColumn("decrypt.file.list.filename.column", "fileName", 200);

        progressCol = new TableColumn<>(messages.getString("decrypt.file.list.progress.column"));
        progressCol.setPrefWidth(200);

        table.getColumns().addAll(fileNameCol, statusCol, progressCol);

        return table;
    }

    boolean markOverwrites() {

        final List<DecryptionTask> decryptionTasks = table.getItems()
                .stream()
                .filter(task -> new File(task.getOriginalFileName()).exists())
                .collect(Collectors.toList());

        final SkipDialog skipDialog = new SkipDialog(messages);

        boolean skipRemaining = false;
        for(DecryptionTask decryptionTask : decryptionTasks){
            if(skipRemaining){
                decryptionTask.skip();
            } else {
                skipDialog.setHeaderText(String.format(messages.getString("decrypt.task.overwrite"), decryptionTask.getOriginalFileName()));
                final Optional optional = skipDialog.showAndWait();
                if(optional.isPresent()){
                    if(skipDialog.getButtonTypeSkipRemaining().equals(optional.get())){
                        skipRemaining = true;
                    } else if(skipDialog.getButtonTypeSkip().equals(optional.get())){
                        decryptionTask.skip();
                    } else if(skipDialog.getButtonTypeYesAll().equals(optional.get())){
                        return true;
                    }
                } else {
                    //cancel
                    return false;
                }
            }
        }

        return true;

    }

    void decryptAllFiles(final String pass) {
        if(markOverwrites()) {
            Platform.runLater(this::toggleControlsDuringDecrypt);
            final ObservableList<DecryptionTask> items = table.getItems();
            ExecutorService executor = Executors.newFixedThreadPool(items.size(), runnable -> {
                final Thread t = new Thread(runnable);
                t.setDaemon(true);
                return t;
            });
            items.forEach(task -> task.setPassword(pass));
            items.forEach(decryptionTask -> decryptionTask.setOnSucceeded(event -> onDecryptionTaskSucceeded(items)));
            items.forEach(executor::execute);
        }
    }

    private void onDecryptionTaskSucceeded(ObservableList<DecryptionTask> items) {
        if(items.stream().filter(FutureTask::isDone).collect(Collectors.toList()).size() == items.size()) {
            Platform.runLater(this::toggleControlsAfterDecrypt);
        }
    }

    private void toggleControlsAfterDecrypt() {
        this.decryptAddFilesButton.setDisable(false);
        this.decryptButton.setDisable(true);
    }

    private void toggleControlsDuringDecrypt() {
        this.decryptAddFilesButton.setDisable(true);
        this.decryptButton.setDisable(true);
    }


    void onClickAddFilesButton(ActionEvent actionEvent) {

        final FileChooser fileChooser = new FileChooser();

        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Cipher File Archive", "*.cfx"));

        final List<File> files = fileChooser.showOpenMultipleDialog(SpringApplication.getPrimaryStage());
        if(files != null) {
            if (table == null) {
                table = createDecryptTaskTable();
                decryptTableContainer.getChildren().add(table);
                table.prefWidthProperty().bind(decryptTableContainer.widthProperty());
                table.prefHeightProperty().bind(decryptTableContainer.heightProperty());
                progressCol.setCellValueFactory(new PropertyValueFactory<>("progress"));
                progressCol.setCellFactory(ProgressBarTableCell.forTableColumn());
            }
            files.forEach(this::addDecryptionTaskToTable);
            decryptButton.setDisable(false);
        }

    }

    public void onClickRemoveFilesButton(ActionEvent actionEvent) {
        final List<String> removals = table.getSelectionModel().getSelectedItems()
                .stream().map(DecryptionTask::getFileName).collect(Collectors.toList());

        final List<DecryptionTask> encryptionTasks = table.getItems()
                .filtered(encryptionTask -> removals.contains(encryptionTask.getFileName()))
                .stream().collect(Collectors.toList());


        for (DecryptionTask decryptionTask : encryptionTasks) {
            table.getItems().remove(decryptionTask);
        }

    }


    private void addDecryptionTaskToTable(File file) {
        try {
            table.getItems().add(new DecryptionTask(file.getCanonicalPath(), this.messages,
                    configurationService.getUserConfiguration()));
        } catch (IOException e) {
            alertDisplayService.showNonModalException(e);
        }
    }


    public void setDecryptTableContainer(AnchorPane decryptTableContainer) {
        this.decryptTableContainer = decryptTableContainer;
    }


    public void setDecryptButton(Button decryptButton) {
        this.decryptButton = decryptButton;
    }

    public void setDecryptAddFilesButton(Button decryptAddFilesButton) {
        this.decryptAddFilesButton = decryptAddFilesButton;
    }


    public void setDecryptShowFileButton(Button decryptShowFileButton) {
        this.decryptShowFileButton = decryptShowFileButton;
    }

    public void setDecryptRemoveFilesButton(Button decryptRemoveFilesButton) {
        this.decryptRemoveFilesButton = decryptRemoveFilesButton;
    }


    public void onClickShowFileButton(ActionEvent actionEvent) {
        if (selectedDecryptionTask != null) {
            try {
                Desktop.getDesktop().open(new File(selectedDecryptionTask.getFileName()).getParentFile());
            } catch (IOException e) {
                alertDisplayService.showNonModalException(e);
            }
        }
    }
}
