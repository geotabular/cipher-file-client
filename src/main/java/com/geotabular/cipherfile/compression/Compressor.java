package com.geotabular.cipherfile.compression;

import com.geotabular.cipherfile.encryption.ProgressCallback;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


public class Compressor {

    public void zip(String zipFile, List<String> fileList, ProgressCallback progressCallback){

        final byte[] buffer = new byte[1024];

        try{

            final FileOutputStream fos = new FileOutputStream(zipFile);
            final ZipOutputStream zos = new ZipOutputStream(fos);
            for(final String file : fileList){
                progressCallback.onProgress(fileList.indexOf(file) + 1, fileList.size(), ProgressCallback.STATUS.Archiving);
                final ZipEntry ze = new ZipEntry(new File(file).getName());
                zos.putNextEntry(ze);
                final FileInputStream in = new FileInputStream(file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            zos.close();
        }catch(IOException ex){
            throw new CompressionException(ex);
        }

    }

    public String getFirstFileName(String zipFile){
        try{
            final ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            final ZipEntry ze = zis.getNextEntry();
            return new File(zipFile).getParent() + File.separator + FilenameUtils.getBaseName(FilenameUtils.getBaseName(ze.getName()));
        }catch(IOException ex){
            throw new CompressionException(ex);
        }
    }

    public List<String> unZip(String zipFile, String outputFolder, ProgressCallback callback) {

        final byte[] buffer = new byte[1024];
        final List<String> files = new ArrayList<>();
        try{
            final File folder = new File(outputFolder);
            if(!folder.exists()){
                final boolean mkDirs = folder.mkdirs();
                if(!mkDirs){
                    throw new CompressionException("Unable to create directory" + folder.getCanonicalPath());
                }
            }
            final ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            final int size = new ZipFile(zipFile).size();
            ZipEntry ze = zis.getNextEntry();
            int count = 1;
            while(ze!=null) {

                final String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                callback.onProgress(count++, size, ProgressCallback.STATUS.Expanding);

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder

                final File parentDir = new File(newFile.getParent());

                if (!parentDir.exists()){
                    final boolean mkdirs = parentDir.mkdirs();
                    if (!mkdirs) {
                        throw new RuntimeException("Unable to extract files");
                    }
                }

                final FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                files.add(newFile.getCanonicalPath());
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        }catch(IOException ex){
            throw new CompressionException(ex);
        }
        return files;

    }
}
