package com.geotabular.cipherfile.compression;

import java.io.IOException;

public class CompressionException extends RuntimeException {

    public CompressionException(IOException ex) {
        super(ex);
    }

    public CompressionException(String message) {
        super(message);
    }
}
