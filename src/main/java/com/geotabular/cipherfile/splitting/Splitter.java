package com.geotabular.cipherfile.splitting;

import com.geotabular.cipherfile.encryption.ProgressCallback;
import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Splitter {


    /**
     * split the file specified by filePath into pieces, each of size
     * chunkSize except for the last one, which may be smaller
     */
    public List<String> split(final String filePath, final long chunkSize, final ProgressCallback callback) throws IOException
    {
        // open the file
        final BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePath));

        // get the file length
        final File file = new File(filePath);
        final long fileSize = file.length();
        
        final String tmpDir = file.getParent() + File.separator  + ".cff" + File.separator + UUID.randomUUID().toString();
        final File tmpPath = new File(tmpDir);
        if(!tmpPath.exists()){
            if(!tmpPath.mkdirs()){
                throw new RuntimeException("Unable to create temporary directory");
            }
        }
        
        if(fileSize <= chunkSize) {
            final String tmpFileName = tmpDir + File.separator + file.getName() + ".0";
            FileUtils.copyFile(file, new File(tmpFileName));
            return Collections.singletonList(tmpFileName);
        }

        final List<String> splitFiles = new ArrayList<>();

        // loop for each full chunk
        int subFile;
        for (subFile = 0; subFile < fileSize / chunkSize; subFile++) {
            callback.onProgress(subFile + 1, Math.toIntExact(fileSize/chunkSize) + 1, ProgressCallback.STATUS.Splitting);
            // open the output file
            final String tmpFileName = tmpDir + File.separator + file.getName()  + "." + subFile;
            final FileOutputStream outFile = new FileOutputStream(tmpFileName);
            final BufferedOutputStream out = new BufferedOutputStream(outFile);

            // write the right amount of bytes
            for (int currentByte = 0; currentByte < chunkSize; currentByte++)
            {
                // load one byte from the input file and write it to the output file
                out.write(in.read());
            }

            // close the file
            out.close();
            splitFiles.add(tmpFileName);
        }

        // loop for the last chunk (which may be smaller than the chunk size)
        if (fileSize != chunkSize * (subFile - 1)) {
            callback.onProgress(subFile + 1, Math.toIntExact(fileSize/chunkSize) + 1,ProgressCallback.STATUS.Splitting);
            // open the output file
            final String tmpFileName = tmpDir + File.separator + file.getName()  + "." + subFile;
            final BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(tmpFileName));

            // write the rest of the file
            int b;
            while ((b = in.read()) != -1)
                out.write(b);

            // close the file
            out.close();
            splitFiles.add(tmpFileName);
        }

        // close the file
        in.close();
        return splitFiles;
    }

    /**
     * list all files in the directory specified by the baseFilename
     * , find out how many parts there are, and then concatenate them
     * together to create a file with the filename <I>baseFilename</I>.
     */
    public void join(String baseFilename, ProgressCallback progressCallback) throws IOException
    {
        final int numberParts = getNumberParts(baseFilename);
        final BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(baseFilename));
        for (int part = 0; part < numberParts; part++)
        {
            final BufferedInputStream in = new BufferedInputStream(new FileInputStream(baseFilename + "." + part));
            progressCallback.onProgress(part + 1, numberParts , ProgressCallback.STATUS.Joining);
            int b;
            while ( (b = in.read()) != -1 )
                out.write(b);

            in.close();
        }
        out.close();
    }

    /**
     * find out how many chunks there are to the base filename
     */
    private static int getNumberParts(String baseFilename) throws IOException
    {
        // list all files in the same directory
        File directory = new File(baseFilename).getAbsoluteFile().getParentFile();
        final String justFilename = new File(baseFilename).getName();
        String[] matchingFiles = directory.list((dir, name) -> name.startsWith(justFilename) && name.substring(justFilename.length()).matches("^\\.\\d+$"));
        return matchingFiles.length;
    }


}
