package com.geotabular.cipherfile.dialogs;

import com.geotabular.cipherfile.i18n.Messages;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

@Service
public class DialogAlertDisplayService implements AlertDisplayService {

    @Autowired
    private Messages messages;

    private void addFavicon(Alert dialog){

        final Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/favicon.png")));

    }

    @Override
    public void showNonModalError(String header, String message){

        Alert alert = new Alert(Alert.AlertType.ERROR);
        showNonModalAlert(header, message, alert);

    }

    @Override
    public void showNonModalInfo(String header, String message){

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(messages.getString("info.dialog.title"));
        alert.setHeaderText(header);
        alert.setContentText(message);
        addFavicon(alert);
        alert.showAndWait();

    }

    private Optional<ButtonType> showNonModalAlert(String header, String message, Alert alert) {
        alert.setTitle(messages.getString("error.dialog.title"));
        alert.setHeaderText(header);
        alert.setContentText(message);
        addFavicon(alert);
        return alert.showAndWait();
    }

    @Override
    public void showExceptionAndExit(Throwable t, String message) {
        displayExceptionMessage(t, message, messages.getString("error.exception.header"));
        System.exit(1);
    }

    private void displayExceptionMessage(Throwable t, String message, String header) {
        final Alert alert = new Alert(Alert.AlertType.ERROR);
        addFavicon(alert);
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);

        alert.setTitle(messages.getString("error.dialog.title"));
        alert.setHeaderText(header);
        alert.setContentText(message);

// Create expandable Exception.

        t.printStackTrace(pw);
        final String exceptionText = sw.toString();
        final Label label = new Label(messages.getString("error.exception.stacktrace"));
        final TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        final GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

// Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();
    }

    @Override
    public void showNonModalException(Throwable e) {
        this.displayExceptionMessage(e, e.toString(), messages.getString("error.exception.header"));
    }

    @Override
    public Optional<ButtonType> showNonModalConfirm(String message) {
        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        return showNonModalAlert(messages.getString("dialog.confirmheader"), message, alert);
    }

    @Override
    public Optional<ButtonType> showNonModalContinueWarning(String message) {

        final ButtonType continueButton = new ButtonType(messages.getString("warning.dialog.continue.button"), ButtonBar.ButtonData.OK_DONE);
        final ButtonType exitButton = new ButtonType(messages.getString("warning.dialog.exit.button"), ButtonBar.ButtonData.CANCEL_CLOSE);
        final Alert alert = new Alert(Alert.AlertType.WARNING, message, continueButton, exitButton);
        alert.setTitle(messages.getString("error.dialog.title"));
        alert.setHeaderText(messages.getString("dialog.confirmheader"));
        addFavicon(alert);
        return alert.showAndWait();

    }
}
