package com.geotabular.cipherfile.dialogs;

import com.geotabular.cipherfile.i18n.Messages;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class SkipDialog extends Dialog {


    private final ButtonType buttonTypeSkip;
    private final ButtonType buttonTypeSkipRemaining;
    private ButtonType buttonTypeAll;
    private ButtonType buttonTypeYes;
    private ButtonType buttonTypeCancel;

    public SkipDialog(Messages messages) {

        setTitle(messages.getString("skip.dialog.title"));
        setContentText(messages.getString("skip.dialog.content"));

        buttonTypeYes = new ButtonType(messages.getString("skip.dialog.button.yes"));
        buttonTypeAll = new ButtonType(messages.getString("skip.dialog.button.overwriteall"));
        buttonTypeSkip = new ButtonType(messages.getString("skip.dialog.button.skip"));
        buttonTypeSkipRemaining = new ButtonType(messages.getString("skip.dialog.button.skipremaining"));
        buttonTypeCancel = new ButtonType(messages.getString("skip.dialog.button.cancel"), ButtonBar.ButtonData.CANCEL_CLOSE);

        getDialogPane().getButtonTypes().setAll(buttonTypeYes, buttonTypeAll, buttonTypeSkip, buttonTypeSkipRemaining, buttonTypeCancel);
        final Stage stage = (Stage)getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/favicon.png")));

    }


    public ButtonType getButtonTypeYesAll() {
        return buttonTypeAll;
    }

    public ButtonType getButtonTypeSkip() {
        return buttonTypeSkip;
    }

    public ButtonType getButtonTypeSkipRemaining() {
        return buttonTypeSkipRemaining;
    }

    public ButtonType getButtonTypeCancel() {
        return buttonTypeCancel;
    }

    public ButtonType getButtonTypeYes() {
        return buttonTypeYes;
    }

}
