package com.geotabular.cipherfile.dialogs;

import javafx.scene.control.ButtonType;

import java.util.Optional;

public interface AlertDisplayService {

    void showNonModalError(String header, String message);

    void showNonModalInfo(String header, String message);

    void showExceptionAndExit(Throwable t, String message);

    void showNonModalException(Throwable e);

    Optional<ButtonType> showNonModalConfirm(String message);

    Optional<ButtonType> showNonModalContinueWarning(String message);
}
