package com.geotabular.cipherfile.dialogs;

import com.geotabular.cipherfile.configuration.ConfigurationService;
import com.geotabular.cipherfile.i18n.Messages;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Optional;
import java.util.regex.Pattern;

public class EncryptPasswordDialog extends AbstractPasswordDialog {

    private final PasswordField passwordField;

    private final static String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,}$";

    private final static String MED_PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";

    private final ConfigurationService configurationService;

    public EncryptPasswordDialog(ConfigurationService configurationService, Messages messages){
        final Stage stage = (Stage)this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/favicon.png")));
        this.configurationService = configurationService;
        setTitle(messages.getString("encrypt.password.dialog.title"));
        setHeaderText(messages.getString("encrypt.password.dialog.header"));
        passwordField = new PasswordField();
        getDialogPane().setContent(buildContent(messages));
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
    }

    private Node buildContent(Messages messages){

        final Label passwordStrengthLabel = new Label("");
        final Label passwordLabel = new Label(messages.getString("encrypt.password.dialog.password.label"));
        final Label passwordPolicyLabel = new Label(messages.getString("encrypt.password.dialog.password.policy"));
        final Label keyLengthLabel = new Label(messages.getString("decrypt.password.dialog.keylength"));
        final Label keyLengthWarningLabel = new Label(messages.getString("decrypt.password.dialog.keylength.warning"));
        final ChoiceBox<Integer> integerChoiceBox = buildKeyLengthChoiceBox(this.configurationService);

        final Insets insets = new Insets(4, 4, 4, 4);
        passwordPolicyLabel.setPadding(insets);
        keyLengthWarningLabel.setPadding(insets);

        final VBox vBox = new VBox();
        vBox.setPadding(new Insets(10, 0, 0, 10));
        vBox.setSpacing(10);

        final HBox keyLengthHb = new HBox();
        keyLengthHb.setSpacing(10);
        keyLengthHb.setAlignment(Pos.CENTER_LEFT);
        keyLengthHb.getChildren().addAll(keyLengthLabel, integerChoiceBox);

        final HBox passStrengthHb = new HBox();
        passStrengthHb.setSpacing(10);
        passStrengthHb.setAlignment(Pos.CENTER_LEFT);
        passwordField.textProperty().addListener((observable, oldValue, newValue) -> {
            final String passwordFieldText = passwordField.getText();
            if (!Pattern.matches(PASSWORD_REGEX, passwordFieldText)) {
                if(!Pattern.matches(MED_PASSWORD_REGEX, passwordFieldText)){
                    passwordStrengthLabel.setText(messages.getString("encrypt.password.dialog.strength.weak"));
                    passwordStrengthLabel.setTextFill(Color.rgb(210, 39, 30));
                } else {
                    passwordStrengthLabel.setText(messages.getString("encrypt.password.dialog.strength.medium"));
                    passwordStrengthLabel.setTextFill(Color.rgb(255, 165, 0));
                }
            } else {
                passwordStrengthLabel.setText(messages.getString("encrypt.password.dialog.strength.strong"));
                passwordStrengthLabel.setTextFill(Color.rgb(21, 117, 84));
            }
        });

        final TextField textField = new TextField();
        final CheckBox checkBox = new CheckBox(messages.getString("password.dialog.show.hide"));
        textField.setManaged(false);
        textField.setVisible(false);
        textField.managedProperty().bind(checkBox.selectedProperty());
        textField.visibleProperty().bind(checkBox.selectedProperty());
        passwordField.managedProperty().bind(checkBox.selectedProperty().not());
        passwordField.visibleProperty().bind(checkBox.selectedProperty().not());
        textField.textProperty().bindBidirectional(passwordField.textProperty());

        passStrengthHb.getChildren().addAll(passwordLabel, textField, passwordField, checkBox);
        vBox.getChildren().addAll(passStrengthHb, passwordStrengthLabel, keyLengthHb, keyLengthWarningLabel, passwordPolicyLabel);
        return vBox;

    }

    public Optional<String> getPassword(){

      return this.showAndWait().filter(ButtonType.OK::equals).map(type -> passwordField.getText() == "" ? null : passwordField.getText());

    }
}
