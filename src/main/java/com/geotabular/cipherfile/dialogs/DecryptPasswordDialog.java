package com.geotabular.cipherfile.dialogs;


import com.geotabular.cipherfile.configuration.ConfigurationService;
import com.geotabular.cipherfile.i18n.Messages;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Optional;

public class DecryptPasswordDialog extends AbstractPasswordDialog {

    private final PasswordField passwordField;
    private final ConfigurationService configurationService;

    public DecryptPasswordDialog(ConfigurationService configurationService, Messages messages){
        final Stage stage = (Stage)this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/favicon.png")));
        this.configurationService = configurationService;
        setTitle(messages.getString("decrypt.password.dialog.title"));
        setHeaderText(messages.getString("decrypt.password.dialog.header"));
        passwordField = new PasswordField();
        getDialogPane().setContent(buildContent(messages));
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
    }

    private Node buildContent(Messages messages){

        final Label passwordLabel = new Label(messages.getString("decrypt.password.dialog.password.label"));
        final Label policyLabel = new Label(messages.getString("decrypt.password.dialog.message"));
        final Label keyLengthLabel = new Label(messages.getString("decrypt.password.dialog.keylength"));
        final ChoiceBox<Integer> integerChoiceBox = buildKeyLengthChoiceBox(this.configurationService);
        policyLabel.setPadding(new Insets(4, 4, 4, 4));
        final VBox vb = new VBox();
        final HBox passwordHb = new HBox();
        final HBox keyLengthHb = new HBox();

        vb.setPadding(new Insets(10, 0, 0, 10));
        vb.setSpacing(10);

        final TextField textField = new TextField();
        final CheckBox checkBox = new CheckBox(messages.getString("password.dialog.show.hide"));
        textField.setManaged(false);
        textField.setVisible(false);
        textField.managedProperty().bind(checkBox.selectedProperty());
        textField.visibleProperty().bind(checkBox.selectedProperty());
        passwordField.managedProperty().bind(checkBox.selectedProperty().not());
        passwordField.visibleProperty().bind(checkBox.selectedProperty().not());
        textField.textProperty().bindBidirectional(passwordField.textProperty());

        passwordHb.setSpacing(10);
        passwordHb.setAlignment(Pos.CENTER_LEFT);
        passwordHb.getChildren().addAll(passwordLabel,textField, passwordField);

        keyLengthHb.setSpacing(10);
        keyLengthHb.setAlignment(Pos.CENTER_LEFT);
        keyLengthHb.getChildren().addAll(keyLengthLabel, integerChoiceBox);




        vb.getChildren().addAll(passwordHb, policyLabel, keyLengthHb);
        return vb;

    }

    public Optional<String> getPassword(){

        return this.showAndWait().filter(ButtonType.OK::equals).map(type -> passwordField.getText() == "" ? null : passwordField.getText());

    }
}
