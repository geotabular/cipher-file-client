package com.geotabular.cipherfile.dialogs;


import com.geotabular.cipherfile.configuration.ConfigurationService;
import com.geotabular.cipherfile.configuration.model.KeyCipherConfiguration;
import com.geotabular.cipherfile.configuration.model.UserConfiguration;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;

import java.util.List;
import java.util.stream.Collectors;

class AbstractPasswordDialog extends Dialog{

    ChoiceBox<Integer> buildKeyLengthChoiceBox(ConfigurationService configurationService) {
        final ChoiceBox<Integer> integerChoiceBox = new ChoiceBox<>();
        final KeyCipherConfiguration keyCipherConfiguration = configurationService.getUserConfiguration().getKeyCipherConfiguration();
        final List<Integer> choiceList = keyCipherConfiguration.getKeyLengthChoices().stream().map(integer ->  integer * 4).collect(Collectors.toList());
        integerChoiceBox.getItems().addAll(choiceList);
        final int configuredKeyLength = keyCipherConfiguration.getDerivedKeyLength() * 4;
        integerChoiceBox.getSelectionModel().select(choiceList.indexOf(configuredKeyLength));
        integerChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable1, oldValue1, newValue1) -> {
            final UserConfiguration userConfiguration = configurationService.getUserConfiguration();
            userConfiguration.getKeyCipherConfiguration().setDerivedKeyLength(newValue1 / 4);
            configurationService.saveUserConfiguration(userConfiguration);
        });
        return integerChoiceBox;
    }

}
