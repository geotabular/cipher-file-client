package com.geotabular.cipherfile;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
@Aspect
public class LoggerAspect {

    final Logger log = LoggerFactory.getLogger(LoggerAspect.class);

    @Around("execution(* com.geotabular.cipherfile..*.*(..))")
    public Object logTimeMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug(new StringBuilder()
                .append(joinPoint.getTarget().getClass().getName()).append(".")
                .append(joinPoint.getSignature().getName()).append("(")
                .append(Arrays.asList(joinPoint.getArgs())
                        .stream().map(String::valueOf)
                        .collect(Collectors.joining(","))).append(")").toString());
        return joinPoint.proceed();
    }

}
